<?php

if(!defined('APP_DIR')) die('Error: Undefined App');

error_reporting(E_ALL ^ E_NOTICE);
define('DS', '/');

require_once dirname(__FILE__).'/libs/sfYaml/sfYaml.php';

/**
Load Config
*/
$app_config = sfYaml::load(APP_DIR.'/app.yml');

$config = sfYaml::load(APP_DIR.'/configs/config'.(empty($app_config['mode'])? '' : '.'.$app_config['mode']).'.yml');
$config['app'] = $app_config['app'];
$config['basefolder'] = $app_config['basefolder'];
$config['website_url'] = ($_SERVER['HTTPS'] ? 'https://' : 'http://')
        .($config['website']['domain'] ? $config['website']['domain'].DS : $_SERVER['SERVER_NAME'].($config['basefolder'] ? DS.$config['basefolder'].DS : DS));

$config['app_dir'] = APP_DIR;
$config['app_url'] = $config['website_url'];
$config['theme_dir'] = $config['app_dir'].DS.'themes'.DS.$config['website']['theme'];
$config['theme_url'] = $config['website_url'].'themes'.DS.$config['website']['theme'];

$routing = sfYaml::load($config['app_dir'].'/configs/routing.yml');

global $request, $response;
$response['data'] = Array();

/**
Set timezone
*/
date_default_timezone_set($app_config['timezone']);

require_once dirname(__FILE__).'/entities/user.php';
require_once dirname(__FILE__).'/daos/dao.sessions.php';
require_once dirname(__FILE__).'/includes/session.php';
require_once dirname(__FILE__).'/includes/twig.php';
require_once dirname(__FILE__).'/includes/func.php';			
require_once dirname(__FILE__).'/includes/htmlhelper.php';
require_once dirname(__FILE__).'/includes/event.php';
require_once dirname(__FILE__).'/includes/db.php';

/**
Connect to database
*/
if(!empty($config['db']['host'])){
	$db = new db();
	if(!$db->connect($config['db']['host'], $config['db']['user'], $config['db']['pwd'], $config['db']['dbname'], $config['db']['pre'])){
		exit_by_error('Unable connect to database');
	}
}
unset($config['db']);

/**
Register Session
*/
$sess_key = !empty($_GET['sess_key']) ? $_GET['sess_key'] : (!empty($_POST['sess_key']) ? $_POST['sess_key'] : ($_REQUEST['device'] == 'mobile' ? '' : $_COOKIE['sess_key']));
sess::register($sess_key);
event::add('end_response', array('sess', 'save'));

/**
Load plugins
*/
foreach($config['website']['plugins'] as $plugin_name){
	require_once dirname(__FILE__).'/plugins/'.$plugin_name.DS.$plugin_name.'.php';
}

/**
Handle Request
*/
if(empty($request['router'])){
	$request['path'] = add_first_slash(str_replace_first(DS.$config['basefolder'], '', empty($_SERVER['SCRIPT_URL']) ? current(explode('?', $_SERVER['REQUEST_URI'])) : current(explode('?', $_SERVER['SCRIPT_URL']))));
	foreach($routing as $router => $pathdata){
		if(!empty($pathdata['pattern']) && preg_match('#'.$pathdata['pattern'].'#', $request['path'], $vars)){
			$request['router'] = $router;
			if (isset($pathdata['params']) && ($n = count($pathdata['params'])) > 0) {
				for ($i = 0; $i < $n; $i++){
					$request['params'][$pathdata['params'][$i]] = $vars[$i + 1];
				}
			}
			break;
		}
	}
}

$response['data'] = &$config;
$response['data']['routing'] = &$routing;
$response['data']['request'] = &$request;

$user = new User();
$user->addRoles(sess::get('member-roles'));
$user->addRole('guest');
$response['data']['user'] = &$user;

/**
Call app.php
*/ 
require_once $config['app_dir'].DS.'app.php';

/**
Dispatch request to controller
*/

if(!empty($request['router'])){
	require_once $config['app_dir'].DS.$routing[$request['router']]['controller'];
}

/**
Call event
*/
event::notify('authentication', array('user' => &$user, 'request' => &$request, 'response' => &$response));
event::notify('firewall', array('user' => &$user, 'request' => &$request, 'response' => &$response));
event::notify('before_response', array('user' => &$user, 'request' => &$request, 'response' => &$response));
event::notify('after_response', array('user' => &$user, 'request' => &$request, 'response' => &$response));
event::notify('end_response', array('user' => &$user, 'request' => &$request, 'response' => &$response));