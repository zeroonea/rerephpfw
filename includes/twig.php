<?php
global $config, $response;
require_once dirname(__FILE__).'/../libs/Twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(array($config['theme_dir'], $config['theme_dir'].DS.'admincp'));
$twig = new Twig_Environment($loader, array(
	'cache' => $config['theme_dir'].DS.'cache',
	'auto_reload' => true,
));
$response['twig'] = $twig;

$twig->addFunction('register_template', new Twig_Function_Function('register_template'));
$twig->addFunction('get_url', new Twig_Function_Function('get_url'));
$twig->addFunction('get_theme_url', new Twig_Function_Function('get_theme_url'));
$twig->addFunction('paging', new Twig_Function_Function('paging'));
$twig->addFunction('time_ago', new Twig_Function_Function('time_ago'));
$twig->addFunction('time_rel', new Twig_Function_Function('time_rel'));
$twig->addFunction('get_current_url', new Twig_Function_Function('get_current_url'));
$twig->addFunction('trim_text', new Twig_Function_Function('trim_text'));
$twig->addFunction('strtotime', new Twig_Function_Function('strtotime'));
$twig->addFunction('get_full_name', new Twig_Function_Function('get_full_name'));
$twig->addFunction('add_ext_css', new Twig_Function_Function('add_ext_css'));
$twig->addFunction('print_all_ext_css', new Twig_Function_Function('print_all_ext_css'));
$twig->addFunction('merge_querystring', new Twig_Function_Function('merge_querystring'));