<?php

function register_template($name, $type, $editable){
	global $config;
	$tmp = explode(DS.$config['app'].DS, $config['theme_dir']);
	
	if($config['website']['devmode'] == 1){
		if($config['website']['tpls'] == null){
			$config['website']['tpls'] = array();
		}
		$config['website']['tpls'][] = array(
			'name' => $tmp[1].DS.$name,
			'type' => $type,
			'editable' => $editable	
		);
	}
}

function redirect($url){
	global $user, $request, $response;
	
	event::notify('end_response', array('user' => &$user, 'request' => &$request, 'response' => &$response));
	header('location: '.$url, true);
	die();
}

function exit_by_error($message, $data = array()){
	global $config;
	$data['id'] = uniqid();
	save_log('error', $message, $data);
	
	if(!$config['website']['devmode']){
		die('Error Occurred (ID:'.$data['id'].'). Please check details in logs/logs.txt ');
	}
}

function save_log($type, $message, $data = array()){
	$data['type'] = $type;
	$data['time'] = date('m-j-Y g:i a');
	$data['message'] = $message;
	
	$log_folder = dirname(__FILE__).'/../logs';
	if(!file_exists($log_folder)){
		mkdir($log_folder, 0755, true);
	}
	
	$fp = fopen($log_folder.'/logs.txt', "a");
	fwrite($fp, json_encode($data)."\n");
	fclose($fp);
}

function get_url($router, $data = array()){
	global $config, $routing;
	$path = $routing[$router]['path'];
	if(empty($path)) return null;
	
	foreach($data as $param_name => $param_value){
		$path = str_replace('{{'.$param_name.'}}', $param_value, $path);
	}
	
	return remove_last_slash($config['website_url']).$path;
}

function get_theme_url($theme = null){
	global $config;
	return $config['theme_url'].DS;
}

function get_request_var($name, $type = 'string', $default = null){
	global $request;
	if(isset($request['params'][$name])){
		$val = $request['params'][$name];
	}else if(isset($_REQUEST[$name])){
		$val = $_REQUEST[$name];
	}else{
		$val = $default;
	}
	
	if($type == 'int'){
		return intval($val);
	}else{
		return $val;
	}
}

/**
 * Get single option value
 *
 * @param string        $option_name
 * @param true|false    $is_unserialize
 * @return mix
 */
function get_option($option_name, $is_unserialize = false) {
    
	$opt = dao_options::get_options($option_name);
	if(!$is_unserialize || !($v = unserialize($opt['option_data']))){
		return $opt['option_data'];
	}
}

/**
 * Get multiple options value
 *
 * @param array         $options_name
 * @param true|false    $is_unserialize
 * @return mix
 */
function get_options($options_name, $is_unserialize = false) {
	$vs = array();
	$opts = dao_options::get_options($options_name);
	foreach($opts as $opt){
		if(!$is_unserialize || !($vs[$opt['option_name']] = unserialize($opt['option_data']))){
			$vs[$opt['option_name']] = $opt['option_data'];
		}
    }
    return $vs;
}

/**
 * Update option
 *
 * @param string            $option_name
 * @param string|array      $value
 * @return option_id
 */
function update_option($option_name, $value) {
    if (is_object($value) || is_array($value)) $value = serialize($value);
    return dao_options::save_option($option_name, $value, MEMBER_ID);
}

/**
 * Remove option
 *
 * @param string $option_name
 * @return int
 */
function unset_option($option_name) {
    return dao_options::remove_option($option_name);
}

/**
 * Create unique id in function level with function arguments and function name
 *
 * @param array $vars
 * @param string $func_name
 * @return string
 */
function fuid($vars, $func_name){
    return md5($func_name . serialize($vars));
}

function deserialize($str){
    if(($tmp = unserialize($str)) != false){
        return $tmp;
    }
    return array();
}

function remove_first_slash($str) {
    return preg_replace('#^[/]+#', '', $str);
}

function remove_last_slash($str) {
    return preg_replace('#[/]+$#', '', $str);
}

function add_last_slash($str) {
    if (strrpos($str, '/') == strlen($str) - 1) {
        return $str;
    }
    return $str . '/';
}

function add_first_slash($str) {
    if (strpos($str, '/') === 0) {
        return $str;
    }
    return '/'.$str;
}

function default_value($val1, $val2){
    if(empty($val1))
        return $val2;
    else
        return $val1;
}

function time_rel($date, $past_tpl = '{{time}} ago', $future_tpl = '{{time}} left', $granularity = 1){
	if(is_string($date)){
		$date = strtotime($date);
	}
	$t = time();
	$tpl = '';
	if($t > $date){
		$difference = $t - $date;
		$tpl = $past_tpl;
	}else{
		$difference = $date - $t;
		$tpl = $future_tpl;
	}
	$periods = array('decade' => 315360000,
        'year' => 31536000,
        'month' => 2628000,
        'week' => 604800, 
        'day' => 86400,
        'hour' => 3600,
        'minute' => 60,
        'second' => 1);
    /*if ($difference < 5) { // less than 5 seconds ago, let's say "just now"
        $retval = 'just now';
        return $retval;
    } else {                            */
        foreach ($periods as $key => $value) {
            if ($difference >= $value) {
                $time = floor($difference/$value);
                $difference %= $value;
                $retval .= ($retval ? ' ' : '').$time.' ';
                $retval .= (($time > 1) ? $key.'s' : $key);
                $granularity--;
            }
            if ($granularity == '0') { break; }
        }
        return str_replace('{{time}}', $retval, $tpl);      
    //}
}

function time_ago($date, $granularity = 1) {
	if(is_string($date)){
		$date = strtotime($date);
	}
    $difference = time() - $date;
    $periods = array('decade' => 315360000,
        'year' => 31536000,
        'month' => 2628000,
        'week' => 604800, 
        'day' => 86400,
        'hour' => 3600,
        'minute' => 60,
        'second' => 1);
    /*if ($difference < 5) { // less than 5 seconds ago, let's say "just now"
        $retval = 'just now';
        return $retval;
    } else {                            */
        foreach ($periods as $key => $value) {
            if ($difference >= $value) {
                $time = floor($difference/$value);
                $difference %= $value;
                $retval .= ($retval ? ' ' : '').$time.' ';
                $retval .= (($time > 1) ? $key.'s' : $key);
                $granularity--;
            }
            if ($granularity == '0') { break; }
        }
        return $retval;      
    //}
}

function get_full_name($first_name, $last_name, $mode = ''){
	if(!empty($first_name) && !empty($last_name)){
		return ucfirst($first_name).' '.strtoupper(substr($last_name, 0, 1)).'.';
	}else if(!empty($first_name)){
		return ucfirst($first_name);
	}else {
		return ucfirst($last_name);
	}
}

/**
 * Replace all flag in url (ex: {website_id}, {{website_url}}) with
 * the given value in $path_vars array
 *
 * @param string $str
 * @param array $path_vars
 * @return string
 */
function replace_vars($str, $path_vars = array()){
    $GLOBALS['__rpv'] = $path_vars;
    return preg_replace_callback('#[\{]{1,2}([^\}\.]*)[\.]?([^\}]*)[\}]{1,2}#', 'replace_vars_callback', $str);
}

function replace_vars_callback($matches){
    if(!empty($matches[2])){
        if($matches[1] == 'path_vars'){
            return path_vars($matches[2]);
        }
    }else if(isset($GLOBALS['__rpv'][$matches[1]])){
        return $GLOBALS['__rpv'][$matches[1]];
    }
    return $matches[0];
}

/**
 * Get current url of current page
 *
 * @param string $query_string
 * @return string
 */
function get_current_url($query_string = '') {
    $url = 'http';
    if ($_SERVER['HTTPS'] == 'on') {$url .= 's';}
        $url .= '://';
    if ($_SERVER['SERVER_PORT'] != '80') {
        $url .= $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
    } else {
        $url .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    }
    if(!empty($query_string))
        return strpos($url, '?') === false ? $url.'?'.$query_string : $url.'&'.$query_string;
    else return $url;
}

/**
 * Get website url
 *
 * @param int|array $website_mix    website id or website array data
 * @return string   website url
 */
function website_url($website_mix, $opts = array('protocol'=>'http')) {
    if($website_mix == WEBSITE_ID || (is_array($website_mix) && $website_mix['website_id'] == WEBSITE_ID)){
        return WEBSITE_URL;
    }
    if (is_numeric($website_mix)) {
        $website = dao_website::getWebsite($website_mix);
    } else if (is_array($website_mix)) {
        $website = $website_mix;
    }

    if (!empty($website['subdomain'])) {
        return $opts['protocol'].'://' . $website['subdomain'] . '.' . simplezt::config('base_host') . DS . simplezt::config('base_folder') . DS;
    } else if (!empty($website['domain'])) {
        return $opts['protocol'].'://' . preg_replace('#^www\.#', '', $website['domain']) . DS . simplezt::config('base_folder') . DS;
    }
}

//[key] => [[path],...]
function sortPath(&$array){
    uasort($array, '_cmp_path');
}

function _cmp_path($a, $b){
    if (strlen($a['path']) == strlen($b['path'])) {
        return 0;
    }
    return (strlen($a['path']) > strlen($b['path'])) ? -1 : 1;
}

function join_string($separate, $parts){
    return preg_replace('#^\\'.$separate.'|\\'.$separate.'$#', '', preg_replace('#[\\'.$separate.']{2,}#', $separate, join($separate, $parts)));
}

function to_oneline($string){
    return preg_replace('#[\n\t\r]*#', '', $string);
}

function merge_querystring($url, $vars = array()){
    $url_info = parse_url($url);
    if(!empty($url_info['query'])){
        parse_str($url_info['query'], $out);
        if(is_array($out)){
            $vars = array_merge($out, $vars);
        }
    }
    $query = http_build_query($vars);
    return $url_info['scheme'].'://'.$url_info['host'].
        (!empty($url_info['port']) ? ':'.$url_info['port'] : '').
        $url_info['path'].(!empty($query) ? '?'.$query : '');
}

function sub($str, $start, $length, $suffix = '...'){
    if(empty($str)) return '';
    if(($n=count($str)) < $start + $length){
        return substr($str, $start, $length).$suffix;
    }else{
        return substr($str, $start);
    }
}

function join_str($str_array, $glue){
    if(is_array($str_array)){
        return implode($glue, $str_array);
    }else return '';
}

/**
 * trims text to a space then adds ellipses if desired
 * @param string $input text to trim
 * @param int $length in characters to trim to
 * @param bool $ellipses if ellipses (...) are to be added
 * @param bool $strip_html if html tags are to be stripped
 * @return string
 */
function trim_text($input, $length, $ellipses = true, $strip_html = true) {
    //strip tags, if desired
    if ($strip_html) {
        $input = strip_tags($input);
    }
  
    //no need to trim, already shorter than trim length
    if (strlen($input) <= $length) {
        return $input;
    }
  
    //find last space within length
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);
  
    //add ellipses (...)
    if ($ellipses) {
        $trimmed_text .= '...';
    }
  
    return $trimmed_text;
}

/**
 * Convert string to slug
 * Ex: This is_a String
 * Result: this-is-a-string
 *
 * @param string $str
 * @return string
 */
function toSlug($str){
    return trim(preg_replace('/-+/', '-', preg_replace('/[^a-z0-9-]/', '-', strtolower(remove_accents($str)))), " \t\r\n\0\x0B-");
}

/**
 * Convert string to a nice string
 * Ex: This is_a String
 * Result: This is a String
 *
 * @param string $str
 * @return string
 */
function toNiceString($str){
    return trim(preg_replace('/ +/', ' ', preg_replace('/[^a-zA-Z0-9 ]/', ' ', $str)));
}

/**
 * Convert string to a safe string
 * (Remove html tag and quotes)
 *
 * @param string $str
 * @return string
 */
function toSafeString($str){
    return str_replace(array('\'', '"'), '', strip_tags($str));
}

/**
 * Convert string to an enum name
 * Ex: This is_a string
 * Result: this_is_a_string
 *
 * @param <type> $str
 * @return <type>
 */
function toEnumName($str){
    return trim(preg_replace('/_+/', '_', preg_replace('/[^a-zA-Z0-9_]/', '_', $str)), " \t\r\n\0\x0B_");
}

/**
 * Convert string to a nice name
 * Ex: This is_a string
 * Result: This Is A String
 *
 * @param string $str
 * @return string
 */
function toNiceName($str){
    return ucwords(trim(preg_replace('/ +/', ' ', preg_replace('/[^a-zA-Z0-9 ]/', ' ', $str))));
}

/**
 * Convert size in bytes to a human readable format
 *
 * @param int $bytes
 * @param int $precision
 * @return string
 */
function bytesToSize($bytes, $precision = 2){
    if($bytes <= 0) return 0;
    $unit = array('B','KB','MB','GB','TB','PB','EB');
    return round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), $precision).' '.$unit[$i];
}

/**
 * Short alias function for global variable
 *
 * @param string $var_name
 * @return mix
 */
function gb($var_name){
    return $GLOBALS[$var_name];
}

/**
 * Get information of user's browser
 *
 * @param string $param
 *
 * @return array    [name, ver]
 */
function get_browser_info($param = null){
    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
    $d = array();

    if(strpos($ua, 'firefox') !== false){
        $d = array('name' => 'firefox');
    }else if(strpos($ua, 'msie') !== false){
        $d = array('name' => 'internet-explorer');
    }else if(strpos($ua, 'chrome') !== false){
        $d = array('name' => 'chrome');
    }else if(strpos($ua, 'opera') !== false){
        $d = array('name' => 'opera');
    }

    if($param != null){
        return $d[$param];
    }else return $d;
}

/**
 * Get file's extension
 *
 * @param string $filename
 * @return string
 */
function file_ext($filename){
    return strtolower(pathinfo($filename, PATHINFO_EXTENSION));
}

function set_cookie($name, $value, $expire = 0, $path = '/', $domain = null, $secure = false, $httponly = false){
    setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
    $_COOKIE[$name] = $value;
}

function send_cookie($name, $value, $expire = 0, $path = '/', $domain = null, $secure = false, $httponly = false){
    if(!$GLOBALS['dispatch']){
        setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
    }
    $_COOKIE[$name] = $value;
}

function remove_cookie($name, $path = '/', $domain = null, $secure = false, $httponly = false){
    setcookie($name, '', time() - 100000, $path, $domain, $secure, $httponly);
    unset($_COOKIE[$name]);
}

/**
 * Check whether an array is assoc array
 *
 * @param array $array
 * @return boolen
 */
function is_assoc($array){
    if(!is_array($array)) return false;
    foreach($array as $k => $v){
        if(is_int($k)) return false;
        else return true;
    }
}

/**
 * Replace string (only one time)
 *
 * @param string $search
 * @param string $replace
 * @param string $subject
 * @return string
 */
function str_replace_first($search, $replace, $subject) {
    $pos = strpos($subject, $search);
    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

function preg_split_trim($pattern, $str){
    $tmp = preg_split($pattern, $str);
    foreach($tmp as &$v){
        $v = trim($v);
    }
    return $tmp;
}

function explode_trim($delimiter, $str){
    $tmp = explode($delimiter, $str);
    foreach($tmp as &$v){
        $v = trim($v);
    }
    return $tmp;
}


/**
 * Replace some utf-8 chars with ascii chars
 *
 * @param string $str
 * @return string
 */
function remove_accents($str){
    $accents = array(
    'à','á','ạ','ả','ã','â','ầ','ấ','ậ','ẩ','ẫ','ă','ằ','ắ','ặ','ẳ','ẵ',
    'è','é','ẹ','ẻ','ẽ','ê','ề','ế','ệ','ể','ễ',
    'ì','í','ị','ỉ','ĩ',
    'ò','ó','ọ','ỏ','õ','ô','ồ','ố','ộ','ổ','ỗ','ơ','ờ','ớ','ợ','ở','ỡ',
    'ù','ú','ụ','ủ','ũ','ư','ừ','ứ','ự','ử','ữ',
    'ỳ','ý','ỵ','ỷ','ỹ',
    'đ',
    'À','Á','Ạ','Ả','Ã','Â','Ầ','Ấ','Ậ','Ẩ','Ẫ','Ă','Ằ','Ắ','Ặ','Ẳ','Ẵ',
    'È','É','Ẹ','Ẻ','Ẽ','Ê','Ề','Ế','Ệ','Ể','Ễ',
    'Ì','Í','Ị','Ỉ','Ĩ',
    'Ò','Ó','Ọ','Ỏ','Õ','Ô','Ồ','Ố','Ộ','Ổ','Ỗ','Ơ','Ờ','Ớ','Ợ','Ở','Ỡ',
    'Ù','Ú','Ụ','Ủ','Ũ','Ư','Ừ','Ứ','Ự','Ử','Ữ',
    'Ỳ','Ý','Ỵ','Ỷ','Ỹ',
    'Đ');

    $ascii = array(
    'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
    'e','e','e','e','e','e','e','e','e','e','e',
    'i','i','i','i','i',
    'o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','o',
    'u','u','u','u','u','u','u','u','u','u','u',
    'y','y','y','y','y',
    'd',
    'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A',
    'E','E','E','E','E','E','E','E','E','E','E',
    'I','I','I','I','I',
    'O','O','O','O','O','O','O','O','O','O','O','O','O','O','O','O','O',
    'U','U','U','U','U','U','U','U','U','U','U',
    'Y','Y','Y','Y','Y',
    'D');

    return str_replace($accents, $ascii, $str);
}

if (!function_exists('array_replace_recursive')) {
    function array_replace_recursive($array, $array1) {
        // handle the arguments, merge one by one
        $args = func_get_args();
        $array = $args[0];
        if (!is_array($array)) {
            return $array;
        }
        for ($i = 1; $i < count($args); $i++) {
            if (is_array($args[$i])) {
                $array = recurse($array, $args[$i]);
            }
        }
        return $array;
    }

    function recurse($array, $array1) {
        foreach ($array1 as $key => $value) {
            // create new key in $array, if it is empty or not an array
            if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key]))) {
                $array[$key] = array();
            }
            // overwrite the value in the base array
            if (is_array($value)) {
                $value = recurse($array[$key], $value);
            }
            $array[$key] = $value;
        }
        return $array;
    }
}

if (get_magic_quotes_gpc ()) {
    function stripslashes_deep($value) {
        $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                stripslashes($value);

        return $value;
    }

    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
	$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}
function email_validation($email){
	if (preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', $email)){
		return true;	
	}
	else{
		return false;
	}
}
function send_mail($name, $from, $to, $subject, $message){
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$headers .= 'From: ' .$name.' <'.$from.'>' . "\r\n";
	
	try{
		@mail($to, $subject, $message, $headers);
		return true;
	}
	catch(Exception $ex){
		return $ex;
	}
}