<?php
class event {
    private static $events;
    private static $callables;

    public static function add($event_name, $callable, $priority = 0){
        if(is_array($callable)){
            $func_name = (is_object($callable[0]) ? get_class($callable[0]).'.' : $callable[0].'::').$callable[1];
            self::$callables[$event_name.'.'.$func_name] = $callable;
        }else{
            $func_name = $callable;
            self::$callables[$event_name.'.'.$func_name] = $func_name;
        }
        self::$events[$event_name][$func_name] = $priority;
    }

    public static function notify($event_name, $params = array()) {
        if (!isset(self::$events[$event_name]) || count(self::$events[$event_name]) <= 0) return null;
        asort(self::$events[$event_name], SORT_NUMERIC);
        $tmp = array_reverse(self::$events[$event_name]);
        foreach ($tmp as $func_name => $priority) {
            if(call_user_func_array(self::$callables[$event_name.'.'.$func_name], $params) === false){
				break;
			}
        }
    }

    public static function notifyFirst($event_name, $params = array()){
        if (count(self::$events[$event_name]) <= 0) return null;
        foreach (self::$events[$event_name] as $func_name => $priority) {
            return call_user_func_array(self::$callables[$event_name.'.'.$func_name], $params);
        }
    }
}
