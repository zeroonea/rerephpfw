<?php

class sess{

	private static $key;
	private static $olddata;
	private static $data;
	
	private static function gen_key(){
		self::$key = md5(uniqid('rere_', true));
		return self::$key;
	}
	
	public static function get_key(){
		return self::$key;
	}

	public static function register($session_key = null){
		global $db;
		
		sess::clearCookie('sess_key');
		
		if(!empty($session_key)){
			self::$key = $session_key;
		}else{
			self::$key = self::gen_key();
		}
		
		if($db){
			$session = dao_sessions::getSessionByKey(self::$key);
			
			if(!empty($session)){
				self::$data = unserialize($session['data']);	
			}else{
				self::clearAllCookies();
				self::$key = self::gen_key();
				self::$data = null;
				if(!dao_sessions::createSession(self::$key, serialize(self::$data), $_SERVER['HTTP_USER_AGENT'])){
					exit_by_error('Cannot create session');
				}
			}
		}
		
		setcookie('sess_key', self::$key, 0, '/');
		
		self::$olddata = self::$data;
	}
	
	private static function clearCookie($name){
		setcookie($name, '', time()-1000);
		setcookie($name, '', time()-1000, '/');
		unset($_COOKIE[$name]);
	}
	
	private static function clearAllCookies(){
		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				setcookie($name, '', time()-1000);
				setcookie($name, '', time()-1000, '/');
				unset($_COOKIE[$name]);
			}
		}
	}
	
	public static function unregister(){
		if(self::$key){
			dao_sessions::deleteSessionByKey(self::$key);
			self::clearAllCookies();
			self::$key = self::$olddata = self::$data = null;
		}
	}
	
	public static function save(){
		if(self::$key && self::$olddata != self::$data){
			if(!dao_sessions::updateSessionByKey(self::$key, serialize(self::$data))){
				exit_by_error('Cannot save session');
			}
		}
	}
	
	public static function getData(){
		return self::$data;
	}

	public static function get($var_name, $default = null, $type = 'string'){
		if(isset(self::$data[$var_name])){
			return self::$data[$var_name];
		}else{
			return $default;
		}
	}
	
	public static function set($var_name, $value){
		self::$data[$var_name] = $value;
		return self::$data[$var_name];
	}
	
	public static function remove($var_name){
		unset(self::$data[$var_name]);
	}
	
	public static function exists($var_name){
		return isset(self::$data[$var_name]);
	}
}