<?php

class db{

	var $queries = array();
	var $dqueries = array();
	var $affected_rows = 0;

	var $link_id = 0;
	var $query_id = 0;

	function connect($server, $user, $pass, $database, $pre=''){
		$this->link_id = @mysql_connect($server, $user, $pass);
		if($this->link_id) {
			if($pre == null) $pre = '';
			$this->pre = $pre;
			if(!mysql_select_db($database, $this->link_id)){
				return false;
			}
			mysql_query('SET NAMES \'UTF8\'', $this->link_id);
			return $this->link_id;
		}
		return false;
	}

	function query($sql) {

		/** Log */
		$tmp = &$this->queries[];
		if(in_array($sql, $this->dqueries)){
			/** Duplicate query */
			$tmp['query'] = '<span style="color:#ff0000">'.$sql.'</span>';
		}else{
			$tmp['query'] = $sql;
		}
		$this->dqueries[] = $sql;
		$tmp['time'] = round(microtime(true) - $GLOBALS['_st'], 3);

		/** Do query */
		$this->query_id = @mysql_query($sql, $this->link_id);
		if (!$this->query_id) {
			exit_by_error(mysql_error());
		}else{
			$this->affected_rows = mysql_affected_rows($this->link_id);
			return $this->query_id;
		}
	}

	function fetch_array($query_id = -1) {
		// retrieve row
		if ($query_id != -1) {
			$this->query_id = $query_id;
		}

		if (isset($this->query_id)) {
			$record = mysql_fetch_assoc($this->query_id);
		}else{
			return null;
		}

		return $record;
	}

	/**
	 * Return all rows
	 *
	 * @param string $sql
	 * @param array $opts   options (key, key1, key2, value, out)
	 * @return array
	 */
	function fetch_all_array($sql, $opts = array()) {
		$query_id = $this->query($sql);
		$out = array();
		if(isset($opts['out'], $opts['key1'], $opts['key2'])) $mode = 1;
		else if(isset($opts['key1'], $opts['key2'])) $mode = 2;
		else if(isset($opts['key'], $opts['array'])) $mode = 3;
		else if(isset($opts['key'])) $mode = 4;
		else $mode = 0;
		
		while($row = $this->fetch_array($query_id)){
			if($mode == 1){
				$opts['out'][$row[$opts['key1']]][$row[$opts['key2']]] = $opts['value'] ? $row[$opts['value']] : $row;
			}else if($mode == 2){
				$out[$row[$opts['key1']]][$row[$opts['key2']]] = $opts['value'] ? $row[$opts['value']] : $row;
			}else if($mode == 3){
				$out[$row[$opts['key']]][] = $opts['value'] ? $row[$opts['value']] : $row;
			}else if($mode == 4){
				$out[$row[$opts['key']]] = $opts['value'] ? $row[$opts['value']] : $row;
			}else{
				$out[] = $opts['value'] ? $row[$opts['value']] : $row;
			}
		}

		$this->free_result($query_id);
		return $out;
	}

	function free_result($query_id = -1){
		if($query_id != -1){
			$this->query_id = $query_id;
		}
		if($this->query_id > 0) {
			mysql_free_result($this->query_id);
		}
	}

	function query_first($query_string){
		$query_id = $this->query($query_string);
		$out = $this->fetch_array($query_id);
		$this->free_result($query_id);
		return $out;
	}

	function update($table, $data, $where='1') {
		$q="UPDATE `".$table."` SET ";

		foreach($data as $key=>$val) {
			if(strtolower($val)=='null') $q.= "`$key` = NULL, ";
			elseif(strtolower($val)=='utc_timestamp()') $q.= "`$key` = UTC_TIMESTAMP(), ";
		elseif(preg_match("/^increment\((\-?\d+)\)$/i",$val,$m)) $q.= "`$key` = `$key` + $m[1], ";
			else $q.= "`$key`='".$this->escape($val)."', ";
		}

		$q = rtrim($q, ', ') . ' WHERE '.$where.';';

		return $this->query($q);
	}


	function insert($table, $data, $ignore_duplicate = false) {
		$q="INSERT ".($ignore_duplicate ? 'IGNORE' : '')." INTO `".$table."` ";
		$v=''; $n='';

		foreach($data as $key=>$val) {
			$n.="`$key`, ";
			if(strtolower($val)=='null') $v.="NULL, ";
			elseif(strtolower($val)=='utc_timestamp()') $v.="UTC_TIMESTAMP(), ";
			else $v.= "'".$this->escape($val)."', ";
		}

		$q .= "(". rtrim($n, ', ') .") VALUES (". rtrim($v, ', ') .");";

		if($this->query($q)){
			return mysql_insert_id($this->link_id);
		}else return false;
	}
	function insert_duplicate($table, $data, $query) {
		$q="INSERT INTO `".$table."` ";
		$v=''; $n='';

		foreach($data as $key=>$val) {
			$n.="`$key`, ";
			if(strtolower($val)=='null') $v.="NULL, ";
			elseif(strtolower($val)=='utc_timestamp()') $v.="UTC_TIMESTAMP(), ";
			else $v.= "'".$this->escape($val)."', ";
		}

		$q .= "(". rtrim($n, ', ') .") VALUES (". rtrim($v, ', ') .") ON DUPLICATE KEY UPDATE ". rtrim($query) .";";

		if($this->query($q)){
			return mysql_insert_id($this->link_id);
		}else return false;
	}
	function __multiple_query($type, $table, $cols, $rows, $ignore_duplicate = true){
		$q= $type.' '.($ignore_duplicate ? 'IGNORE' : '')." INTO `".$table."` ";
		$vs = array();

		foreach($rows as &$row) {
			$v = '';
			foreach($row as $val) {
				if(strtolower($val)=='null') $v .= "NULL, ";
				else if(strtolower($val)=='utc_timestamp()') $v .= "UTC_TIMESTAMP(), ";
				else $v .= "'".$this->escape($val)."', ";
			}

			if(!empty($v)) $vs[] = '('. rtrim($v, ', ') . ')';
		}

		$q .= "(`".join('`, `', $cols)."`) VALUES ".join(', ', $vs).';';
		if($this->query($q)){
			return true;
		}else return false;
	}

	function multiple_insert($table, $cols, $rows, $ignore_duplicate = true){
		return $this->__multiple_query('INSERT ', $table, $cols, $rows, $ignore_duplicate);
	}

	function replace_into($table, $cols, $rows){
		return $this->__multiple_query('REPLACE ', $table, $cols, $rows, false);
	}


	function found_rows(){
		$tmp = $this->query_first('SELECT FOUND_ROWS() as total');
		return $tmp['total'];
	}

	function limit($page, $limit, $default_limit = 20){
		if($page != null && $limit != null){
			$page = $page > 0 ? (int) $page : 1;
			$limit = $limit > 0 ? (int) $limit : $default_limit;
			$offset = ($page - 1) * $limit;
			if($offset >= 0 && $limit > 0){
				return " LIMIT $offset, $limit";
			}
		}else if($limit != null){
			$limit = $limit > 0 ? (int) $limit : $default_limit;
			return " LIMIT $limit";
		}
		return '';
	}

	function escape($string){
		return @mysql_real_escape_string(stripslashes($string), $this->link_id);
	}

	function escape_join($glue, $array){
		foreach($array as &$v){
			$v = $this->escape($v);
		}
		return implode($glue, $array);
	}
}