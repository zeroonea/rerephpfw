<?php

function print_time_ago($time, $granularity = 1){
	return '<span data-time="'.$time.'">'.time_ago($time, $granularity).'</span>';
}

/**
 * Generate paging html
 *
 * @param int $current_page    Current page number
 * @param int $page_size       Number of rows per page
 * @param int $total_rows      Total of rows (Found Rows)
 * @param   array   $opts vars      (
		raw_url:string, 
		index_limit:int, 
		text:array, 
		page_size_vname:string, 
		page_vname:string, 
		class:string
	)
 *
 * @return null
 */
function paging($current_page, $page_size, $total_rows, $opts = array()) {
    $opts['page_size_vname'] = empty($opts['page_size_vname']) ? 'limit' : $opts['page_size_vname'];
    $opts['page_vname'] = empty($opts['page_vname']) ? 'page' : $opts['page_vname'];

    if(empty($page_size)) $page_size = get_request_var($opts['page_size_vname'], 'int');
    if(empty($current_page)) $current_page = get_request_var($opts['page_vname'], 'int');
    if(empty($page_size)) return;
    if(empty($current_page)) $current_page = 1;

    if($total_rows <= $page_size) return;
    if(empty($opts['index_limit'])) $opts['index_limit'] = 5;
    if(empty($opts['text']['label'])) $opts['text']['label'] = '';
    if(empty($opts['text']['first'])) $opts['text']['first'] = '<< First';
    if(empty($opts['text']['previous'])) $opts['text']['previous'] = '< Previous';
    if(empty($opts['text']['last'])) $opts['text']['last'] = 'Last >>';
    if(empty($opts['text']['next'])) $opts['text']['next'] = 'Next >';
    if(empty($opts['class'])) $opts['class'] = 'paging clearfix';

    $total_pages = ceil($total_rows / $page_size);
    $start = max($current_page - intval($opts['index_limit'] / 2), 1);
    $end = $start + $opts['index_limit'] - 1;
    if($end > $total_pages) $start = max($start - ($end - $total_pages), 1);
    $url = empty($opts['url']) ? get_current_url() : $opts['url'];

    $html = '<ul class="'.$opts['class'].'">';
    $html .= '<li>Page <span>'.$current_page.'</span> of <span>'.$total_pages.'</span></li>';

    if($start > 1) {
        $i = 1;
        $html .= '<li><a title="" href="'.merge_querystring($url, array($opts['page_vname']=>$i, $opts['page_size_vname']=>$page_size)).'">'.$opts['text']['first'].'</a></li>';
    }
    if($current_page == 1) {
        //echo '<span class="prn">< Previous</span> ';
    } else {
        $i = $current_page - 1;
        $html .= '<li><a rel="nofollow" href="'.merge_querystring($url, array($opts['page_vname']=>$i, $opts['page_size_vname']=>$page_size)).'">'.$opts['text']['previous'].'</a></li>';
        if($start > 1) $html .= '<li><span>...</span></li>';
    }
    for ($i = $start; $i <= $end && $i <= $total_pages; $i++){
        if($i == $current_page) {
            $html .= '<li><span>'.$i.'</span></li>';
        } else {
            $html .= '<li><a href="'.merge_querystring($url, array($opts['page_vname']=>$i, $opts['page_size_vname']=>$page_size)).'">'.$i.'</a></li>';
        }
    }
    if($current_page < $total_pages){
        $i = $current_page + 1;
        if($end < $total_pages){
            $html .= '<li><span>...</span></li>';
        }
        $html .= '<li><a rel="nofollow" href="'.merge_querystring($url, array($opts['page_vname']=>$i, $opts['page_size_vname']=>$page_size)).'">'.$opts['text']['next'].'</a></li>';
        if($total_pages > $end){
            $i = $total_pages;
            $html .= '<li><a href="'.merge_querystring($url, array($opts['page_vname']=>$i, $opts['page_size_vname']=>$page_size)).'">'.$opts['text']['last'].'</a></li>';
        }
    }
    $html .= '</ul>';

    return $html;
}

function add_ext_css($url, $media = '', $stackable = true){
	if(!isset($GLOBALS['ext_css'])) $GLOBALS['ext_css'] = array();
	
	$css = array('url' => $url, 'media' => $media);
	if(!in_array($css, $GLOBALS['ext_css'])){
		$GLOBALS['ext_css'][] = $css;
		if($stackable){
			if(!isset($GLOBALS['css_key'])) $GLOBALS['css_key'] = '';
			$GLOBALS['css_key'] .= md5($url);
		}
	}
}

function print_all_ext_css($stack = true){
	if(is_array($GLOBALS['ext_css'])){
		if($stack){
			global $config;
			$GLOBALS['css_key'] = md5($GLOBALS['css_key']);
			$cached_css_file = $config['theme_dir'].DS.'cache'.DS.$GLOBALS['css_key'].'.css';
			if(!file_exists($cached_css_file) || $config['website']['devmode'] == 1){
				include_once dirname(__FILE__).'/../libs/rerephpclasses/libs/simple_html_dom.php';
				include_once dirname(__FILE__).'/../libs/rerephpclasses/rerewebparser.php';
				$parser = new rerewebparser();
				$results = $parser->parse_css_files($config['website_url'], $GLOBALS['ext_css']);
				$css_str = '';
				foreach($results as $result){
					if(is_array($result['rules'])){
						foreach($result['rules'] as $rule){
							if(!$rule['rules']){
								$css_str .= $rule['n'] . '{' . "\n" . $rule['v'] . "\n" . '}' . "\n";
							}else{
								$css_str .= $rule['n'] . '{' . "\n";
								foreach($rule['rules'] as $rule_){
									$css_str .= $rule_['n'] . '{' . "\n" . $rule_['v'] . "\n" . '}' . "\n";
								}
								$css_str .= '}' . "\n"; 
							}
						}
					}
				}
				$fp = fopen($cached_css_file, 'w');
				fwrite($fp, $css_str);
				fclose($fp);
			}
			
			return '<link rel="stylesheet" href="'.get_theme_url().'cache'.DS.$GLOBALS['css_key'].'.css">';
		}
		$results = '';
		foreach($GLOBALS['ext_css'] as $css){
			$result .= '<link rel="stylesheet" href="'.$css['url'].'"'.(!empty($css['media']) ? ' media="'.$css['media'].'"' : '').'>'."\n";
		}
		
		return $results;
	}
	return '';
}
