<?php

function googleplus_init($user, $request, $response){
	googleplus_init_user($user, array());
}event::add('authentication', 'googleplus_init', 99);	

function googleplus_init_user($user, $data){
	
	if($user->getID() > 0) return true;
	
	if(empty($data['open_id'])){
		$data['open_id'] = sess::get('open_id');
	}
	
	if(empty($data['open_id'])) return "no_open_id";
	$data['screen_name'] = strtolower($data['screen_name']);
	
	include_once dirname(__FILE__).'/../../daos/dao.members.php';
	$member = dao_members::getMemberByOpenId($data['open_id'], 'googleplus');
	$attrs = array();
	
	if(empty($member)){
		if(empty($data['screen_name'])){
			return "no_screen_name";
		}
		if(dao_members::getMemberByScreenname($data['screen_name'])){
			return "screen_name_existed";
		}
		$member_id = dao_members::createNewUser($data['open_id'], 'googleplus', 
			$data['screen_name'], 
			$data['first_name'], 
			$data['last_name'], 
			$data['email'], 
			'', '',
			$data['avatar']);
			
		$user->setID($member_id);
		$user->addRole('new_gp_user');
		$attrs = unserialize($member['attrs']);
	}else{
		if(!empty($data['screen_name'])){
			if(!dao_members::getMemberByScreenname($data['screen_name'])){
				if(dao_members::updateScreenname($member['member_id'], $data['screen_name'])){
					$member['screen_name'] = $data['screen_name'];
				}else{
					return false;
				}
			}else{
				return "screen_name_existed";
			}
		}
		if(empty($member['screen_name'])){
			return "no_screen_name";
		}
		
		$user->setID($member['member_id']);
		
		//TODO: set member data
		$user->setScreenname($member['screen_name']);
		$user->setFirstname($member['first_name']);
		$user->setLastname($member['last_name']);
		$user->setAvatar($member['avatar']);
		$user->setBio($member['bio']);
			
		$user->removeRole('new_gp_user');
	}
	
	sess::set('open_id', $data['open_id']);
	
	if(!empty($data['regid'])){
		$attrs['regid'] = $data['regid'];
		dao_members::updateAttrs($user->getID(), serialize($attrs));
	}
	
	$user->setAttrs($attrs);
	$user->setOpenId($data['open_id']);
	
	$user->removeRole('guest');
	$user->addRole('member');
	$user->addRole('openid_user');
	
	return true;
}