<?php

function gp_user_controller($user, $request, $response){
	global $config;
	
	if($request['router'] == 'googleplus_login'){
		
		$code = googleplus_init_user($user, $_POST);
		
		if($code === true){
			echo json_encode(array(
				'status' => 'success',
				'mid' => $user->getID(),
				'sn' => $user->getScreenname(),
				'fn' => $user->getFirstname(),
				'ln' => $user->getLastname(),
				'bio' => $user->getBio(),
				'sess_key' => sess::get_key()
			));
		}else{
			echo json_encode(array(
				'status' => 'error',
				'code' => $code
			));
		}
	}
}event::add('authentication', 'gp_user_controller', 100);	

function gp_user_controller2($user, $request, $response){
	if($request['router'] == 'googleplus_logout'){
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		
		// Remove google regid
		$attrs = $user->getAttrs();
		$attrs['regid'] = '';
		unset($attrs['regid']);
		dao_members::updateAttrs($user->getID(), serialize($attrs));	
		
		// Delete session
		sess::unregister();
		
		echo json_encode(array(
			'status' => 'success'
		));
	}
}event::add('authentication', 'gp_user_controller2', 99);	

function gp_after_response($user, $request, $response){
	if($request['router'] == 'googleplus_save_profile'){
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		
		dao_members::updateProfile($user->getID(), $_REQUEST['first_name'], $_REQUEST['last_name'], $_REQUEST['bio']);
		
		echo json_encode(array(
			'status' => 'success'
		));
	}
}event::add('after_response', 'gp_after_response');
