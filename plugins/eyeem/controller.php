<?php

function eyeem_user_controller($user, $request, $response){
	global $config, $eyeem;
	include_once dirname(__FILE__).'/api/eyeem.class.php';
	
	if($eyeem == null){
		$eyeem = new Eyeem(array(
			'apiKey'      => $config['eyeem']['client_id'],
			'apiSecret'   => $config['eyeem']['client_secret'],
			'apiCallback' => get_url('eyeem_login_check')
		));
	}
		
	if($request['router'] == 'eyeem_login'){
		redirect($eyeem->getLoginUrl());
	}else if($request['router'] == 'eyeem_login_check'){
		if($_GET['error'] == 'access_denied'){
			redirect(get_url('home'));
		}else if(!empty($_GET['code'])) {
			$data = $eyeem->getOAuthToken($_GET['code']);
			if(empty($data->error_type)){
				sess::set('access_token', $data->access_token);	
				
				//Get user info
				$eyeem->setAccessToken($data->access_token);
				$user_info = $eyeem->getUser();
				
				if($user_info->user){
					sess::set('open_id', $user_info->user->id);
					sess::set('open_id_provider', 'eyeem');
					eyeem_init_user($user);				
				}else{
					//TODO
				}
				
				/*
				echo '<pre>';
				print_r($user_info);
				die();
				*/
			}
		}
	}else if($request['router'] == 'logout'){
		sess::unregister();
	}
}event::add('authentication', 'eyeem_user_controller', 100);	