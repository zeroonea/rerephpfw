<?php
	function eyeem_authentication($user, $request, $response){
		global $config, $eyeem;
		include_once dirname(__FILE__).'/api/eyeem.class.php';
		
		if($eyeem == null){
			$eyeem = new Eyeem(array(
				'apiKey'      => $config['eyeem']['client_id'],
				'apiSecret'   => $config['eyeem']['client_secret'],
				'apiCallback' => get_url('eyeem_login_check')
			));
		}
		eyeem_init_user($user , true);
	
	}event::add('authentication', 'eyeem_authentication', 99);	
	
	function eyeem_init_user(&$user, $flag = false){
		global $config, $eyeem;
		
		if($user -> getID() > 0 || !sess::get('open_id')) return;
		
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		
		$member = dao_members::getMemberByOpenId(sess::get('open_id'), 'eyeem');
		
		if(empty($member)){		
			if(!$flag){
				//$member_id = dao_members::createNewUser(sess::get('open_id'), 'eyeem', '');
				//$user->setID($member_id);
				$user->addRole('unregister-eyeem-user');
			}
		}else{			
			$user->setID($member['member_id']);
			$user->setFirstName($member['first_name']);
			$user->setLastName($member['last_name']);
			$user->setEmail($member['email']);
			$user->removeRole('guest');
			$user->addRole('member');
			$user->addRole('openid_user');			
			$user->addRole('eyeem_user');
			
			sess::set('member_id', $member['member_id']);
		}
	}
	
	function call_eyeem_api($func, $params = array()){
		global $eyeem, $config;

		if($eyeem == null){
			$eyeem = new Eyeem(array(
				'apiKey'      => $config['eyeem']['client_id'],
				'apiSecret'   => $config['eyeem']['client_secret'],
				'apiCallback' => get_url('eyeem_login_check')
			));
		}
		
		$eyeem->setAccessToken(sess::get('access_token'));
		return call_user_func_array(array($eyeem, $func), $params);
	}

	