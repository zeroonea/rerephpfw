<?php

function twitter_user_controller($user, $request, $response){
	global $config, $twitter;
		
	require_once dirname(__FILE__).'/tmhOAuth/tmhOAuth.php';
	
	if($twitter == null){
		$twitter = new tmhOAuth(array(
			'consumer_key' => $config['twitter']['consumer_key'],
			'consumer_secret' => $config['twitter']['consumer_secret'],
			'curl_ssl_verifypeer' => false,
			'curl_ssl_verifyhost' => false
		));
	}
	
	$twitter_tokens = sess::get('twitter_tokens');
	
	if($request['router'] == 'twitter_login'){
		$params = array(
			'oauth_callback' => merge_querystring(get_url('twitter_login_check'), array(
									'result_type' => $_REQUEST['result_type'],
									'sess_key' => sess::get_key()
								))
		);
		
		$code = $twitter->request('POST', $twitter->url('oauth/request_token', ''), $params);
		
		if($code == 200){
			sess::set('twitter_tokens', $twitter->extract_params($twitter->response['response']));
			$twitter_tokens = sess::get('twitter_tokens');
	
			$method = isset($_REQUEST['authenticate']) ? 'authenticate' : 'authorize';
			$force = isset($_REQUEST['force']) ? '&force_login=1' : '';
			$authurl = $twitter->url('oauth/'.$method, '') . '?oauth_token='.$twitter_tokens['oauth_token'].$force;
			
			redirect($authurl);
		}
	}
	else if($request['router'] == 'twitter_login_check' && isset($_REQUEST['oauth_verifier'], $twitter_tokens)){
	
		$twitter->config['user_token'] = $twitter_tokens['oauth_token'];
		$twitter->config['user_secret'] = $twitter_tokens['oauth_token_secret'];
		sess::remove('twitter_tokens');

		$code = $twitter->request('POST', $twitter->url('oauth/access_token', ''), array(
			'oauth_verifier' => $_REQUEST['oauth_verifier']
		));

		if($code == 200){
			$twitter_user_data = $twitter->extract_params($twitter->response['response']);
			twitter_init_user($user, $twitter_user_data);
			
			if($_REQUEST['result_type'] == 'json'){
				echo json_encode($twitter_user_data);
				return false;
			}else if($_REQUEST['result_type'] == 'query_string'){
				return false;
			}
		}
	}
	else if($request['router'] == 'logout'){
		sess::unregister();
	}
	else if($request['router'] == 'get_twitter_token'){
		$params = array(
			'oauth_callback' => merge_querystring(get_url('get_twitter_token_callback'), array(
				'result_type' => $_REQUEST['result_type'],
				'sess_key' => sess::get_key(),
				'return_url' => $_REQUEST['return_url']
			))
		);
		
		$code = $twitter->request('POST', $twitter->url('oauth/request_token', ''), $params);
				
		if($code == 200){
			sess::set('twitter_tokens', $twitter->extract_params($twitter->response['response']));
			$twitter_tokens = sess::get('twitter_tokens');
	
			$method = isset($_REQUEST['authenticate']) ? 'authenticate' : 'authorize';
			$force = isset($_REQUEST['force']) ? '&force_login=1' : '';
			$authurl = $twitter->url('oauth/'.$method, '') . '?oauth_token='.$twitter_tokens['oauth_token'].$force;
			
			redirect($authurl);
		}
	}
	else if($request['router'] == 'get_twitter_token_callback'){
		$twitter->config['user_token'] = $twitter_tokens['oauth_token'];
		$twitter->config['user_secret'] = $twitter_tokens['oauth_token_secret'];
		sess::remove('twitter_tokens');

		$code = $twitter->request('POST', $twitter->url('oauth/access_token', ''), array(
			'oauth_verifier' => $_REQUEST['oauth_verifier']
		));

		if($code == 200){
			$twitter_user_data = $twitter->extract_params($twitter->response['response']);
			sess::set('twitter_user_data', $twitter_user_data);
			
			if($_REQUEST['result_type'] == 'json'){
				echo json_encode($twitter_user_data);
				return false;
			}else if($_REQUEST['result_type'] == 'query_string'){
				return false;
			}
		}
	}
}event::add('authentication', 'twitter_user_controller', 100);	