<?php

function twitter_authentication($user, $request, $response){
	global $config, $twitter;
	
	require_once dirname(__FILE__).'/tmhOAuth/tmhOAuth.php';

	if($twitter == null){
		$twitter = new tmhOAuth(array(
			'consumer_key' => $config['twitter']['consumer_key'],
			'consumer_secret' => $config['twitter']['consumer_secret'],
			'curl_ssl_verifypeer' => false,
			'curl_ssl_verifyhost' => false
		));
	}
	
	twitter_init_user($user, sess::get('twitter_user_data'), true);
	
}event::add('authentication', 'twitter_authentication', 99);	

function twitter_init_user(&$user, $twitter_user_data, $flag = false){
	global $config, $twitter;
	
	if(empty($twitter_user_data['user_id'])) return false;
	
	$twitter->config['user_token'] = $twitter_user_data['oauth_token'];
	$twitter->config['user_secret'] = $twitter_user_data['oauth_token_secret'];	
	
	include_once dirname(__FILE__).'/../../daos/dao.members.php';
	$member = dao_members::getMemberByOpenId($twitter_user_data['user_id'], 'twitter');
		
	if(empty($member)){
		if(!$flag){
			/* Add new twitter user */
			$member_id = dao_members::createNewUser($twitter_user_data['user_id'], 'twitter', $twitter_user_data['screen_name']);
			$user->setID($member_id);
			$user->addRole('new_twitter_user');
		}
	}else{
		$user->setID($member['member_id']);
		$user->addRole('twitter_user');
	}
	
	$user->setOpenId($twitter_user_data['user_id']);
	$user->setOauthToken($twitter_user_data['oauth_token']);
	$user->setOauthTokenSecret($twitter_user_data['oauth_token_secret']);
	$user->setScreenname($twitter_user_data['screen_name']);
	
	$user->removeRole('guest');
	$user->addRole('member');
	$user->addRole('openid_user');
	
	sess::set('twitter_user_data', $twitter_user_data);
}