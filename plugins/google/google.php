<?php

function f_authentication($user, $request, $response){
	global $config, $google;
	
	include_once dirname(__FILE__).'/api/Google_Client.php';	
	if($google == null){
		$google = new Google_Client();
		$google->setClientId($config['google']['client_id']);
		$google->setClientSecret($config['google']['client_secret']);
		$google->setRedirectUri(get_url('google_login_check'));		
		$google->setScopes($config['google']['scopes']);		
	}
	google_init_user($user, null, true);
	
}event::add('authentication', 'f_authentication', 99);	

function google_init_user(&$user, $profile, $flag = false){	
		
	if($user->getID() > 0) return;	
	
	if($profile){
		
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		$member = dao_members::getMemberByOpenId($profile['id'], 'google');
		

		if(empty($member)){
			if(!$flag){
				/* Add new google user */				
				$member_id = dao_members::createNewUser($profile['id'], 'google', '', $profile['name']['givenName'], $profile['name']['familyName'], '','',$profile['language']);
				$user->setID($member_id);
				$user->addRole('new_google_user');
			}
		}else{
			$user->setID($member['member_id']);
			$user->setFirstName($member['first_name']);
			$user->setLastName($member['last_name']);
			$user->setEmail($member['email']);
			$user->removeRole('guest');
			$user->addRole('member');
			$user->addRole('openid_user');			
			$user->addRole('google_user');			
			sess::set('member_id', $member['member_id']);
			
		}
	}
}