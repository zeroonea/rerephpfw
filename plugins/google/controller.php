<?php

function f_user_controller($user, $request, $response){
	global $config, $google;
	
	include_once dirname(__FILE__).'/api/Google_Client.php';
	include_once dirname(__FILE__).'/api/contrib/Google_PlusService.php';
	if($google == null){
		$google = new Google_Client();
		$google->setClientId($config['google']['client_id']);
		$google->setClientSecret($config['google']['client_secret']);
		$google->setRedirectUri(get_url('google_login_check'));		
		$google->setScopes($config['google']['scopes']);
		$plus = new Google_PlusService($google);
	}
	
	if($request['router'] == 'google_login'){
		 $google->authenticate();	
	}else if($request['router'] == 'google_login_check'){		
		if (isset($_GET['code'])) {
			$google->authenticate();			
			$google->setAccessToken($google->getAccessToken());			
						
			$profile = $plus->people->get('me');
			sess::set('google_access_token', $google->getAccessToken());
			sess::set('open_id', $profile['id']);	
			sess::set('open_id_provider', 'google');
			google_init_user($user,$profile);
		}
		
		
	}else if($request['router'] == 'logout'){
		
		$fb_signed_request = 'fbsr_'.$fb->getAppId();
		setcookie($fb_signed_request, '', time() - 1000);
		setcookie($fb_signed_request, '', time() - 1000, '/');
		
		sess::unregister();
	}
}event::add('authentication', 'f_user_controller', 100);	