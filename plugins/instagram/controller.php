<?php

function instagram_user_controller($user, $request, $response){
	global $config, $instagram;
	include_once dirname(__FILE__).'/api/instagram.class.php';
	
	if($instagram == null){
		$instagram = new Instagram(array(
			'apiKey'      => $config['instagram']['client_id'],
			'apiSecret'   => $config['instagram']['client_secret'],
			'apiCallback' => get_url('instagram_login_check')
		));
	}
		
	if($request['router'] == 'instagram_login'){
		$loginUrl = $instagram->getLoginUrl($config['instagram']['permissions']);
		redirect($loginUrl);
	}else if($request['router'] == 'instagram_login_check'){		
		if(!empty($_GET['code'])) {
			$data = $instagram->getOAuthToken($_GET['code']);
			if(empty($data->error_type)){
				sess::set('access_token', $data->access_token);				
				sess::set('open_id', $data->user->id);	
				sess::set('open_id_provider', 'instagram');
				instagram_init_user($user);				
			}
		}
	}else if($request['router'] == 'logout'){
		sess::unregister();
	}
}event::add('authentication', 'instagram_user_controller', 100);	