<?php
	function instagram_authentication($user, $request, $response){
		global $config, $instagram;
		include_once dirname(__FILE__).'/api/instagram.class.php';
		
		if($instagram == null){
			$instagram = new Instagram(array(
				'apiKey'      => $config['instagram']['client_id'],
				'apiSecret'   => $config['instagram']['client_secret'],
				'apiCallback' => get_url('instagram_login_check')
			));
		}
		instagram_init_user($user , true);
	
	}event::add('authentication', 'instagram_authentication', 99);	
	
	function instagram_init_user(&$user, $flag = false){
		global $config, $instagram;
		
		if($user -> getID() > 0 || !sess::get('open_id')) return;
		
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		
		$member = dao_members::getMemberByOpenId(sess::get('open_id'), 'instagram');
		
		if(empty($member)){		
			if(!$flag){
				//$member_id = dao_members::createNewUser(sess::get('open_id'), 'instagram', '');
				//$user->setID($member_id);
				$user->addRole('unregister-instagram-user');
			}
		}else{			
			$user->setID($member['member_id']);
			$user->setFirstName($member['first_name']);
			$user->setLastName($member['last_name']);
			$user->setEmail($member['email']);
			$user->removeRole('guest');
			$user->addRole('member');
			$user->addRole('openid_user');			
			$user->addRole('instagram_user');
			
			sess::set('member_id', $member['member_id']);
		}
	}
	function call_instagram_api($func, $params = array()){
		global $instagram, $config;

		if($instagram == null){
			$instagram = new Instagram(array(
				'apiKey'      => $config['instagram']['client_id'],
				'apiSecret'   => $config['instagram']['client_secret'],
				'apiCallback' => get_url('instagram_login_check')
			));
		}

		$instagram->setAccessToken(sess::get('access_token'));
		return call_user_func_array(array($instagram, $func), $params);
	}

	