<?php

function ipn_linstener(){
	global $config, $paypal;

	include_once dirname(__FILE__).'/api/ipnlistener.php';
	if($config['paypal']['logs_mode']){
		ini_set('log_errors', true);
		ini_set('error_log', dirname(__FILE__).'/logs/ipn_errors.log');
	}
	
	if(empty($paypal)){
		$paypal = new IpnListener();
		if($config['paypal']['sanbox_mode']){
			$paypal->use_sandbox = true;
		}
	}
	// try to process the IPN POST
	try{
		$paypal->requirePostMethod();
		
		$verified = $paypal->processIpn();
	}catch (Exception $e){
		error_log($e->getMessage());
		exit(0);
	}

	if($verified){

		//$errmsg = '';   // stores errors from fraud checks
		
		// 1. Make sure the payment status is "Completed" 
		if($_POST['payment_status'] != 'Completed'){ 
			event::notify('paypal_payment_failed', array('error' => $paypal->getTextReport()));
		}
		else{
			event::notify('paypal_payment_completed', array('invoice_id' => $_POST['invoice'],
															'txn_id' =>  $_POST['txn_id'],
															'payer_email' => $_POST['payer_email'],
															'mc_gross' => $_POST['mc_gross']
															));			
		}
		
	}else{
		event::notify('paypal_invalid_IPN', array('error' => $paypal->getTextReport()));
	}	
}

function paypal_single_payments_standard($seller_email, $item_name, $invoice, $amount, $return_url, $notify_url, $cancel_url, $return_text){
	global $config;
	
	$params = array(
		'cmd' => '_xclick',
		'business' => $seller_email, 
		'currency_code' => $config['paypal']['currency_code'],
		'item_name' => $item_name,
		'invoice' => $invoice, 
		'amount' => $amount,
		'return' => $return_url,
		'notify_url' => $notify_url,
		'cancel_return' => $cancel_url,
		'cbt' => $return_text
	);
	
	redirect($config['paypal']['payments_standard_url'].'?'.http_build_query($params));
}