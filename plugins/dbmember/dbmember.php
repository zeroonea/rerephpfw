<?php
	function db_authentication($user, $request, $response){
		global $config;
					
		db_init_user($user);
	
	}event::add('authentication', 'db_authentication', 99);	
	
	function db_init_user(&$user, $email = '' , $password = ''){
		global $config;
		
		if($user->getID() > 0) return true;
		
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		
		$member_id = sess::get('member_id');
		
		if(!empty($email)){
			// Login
			$member = dao_members::getMemberByEmail($email);
			if($member['password'] != md5($password)){
				return false;
			}
		}else if($member_id > 0){
			// User already login, reload user's data from database
			$member = dao_members::getMemberByID($member_id);
		}
		
		if(!empty($member)){
			$user->setID($member['member_id']);
			$user->setScreenname($member['screen_name']);
			$user->setFirstName($member['first_name']);
			$user->setLastName($member['last_name']);
			$user->setEmail($member['email']);
			$user->removeRole('guest');
			$user->addRole('member');
			
			sess::set('member_id', $member['member_id']);
		}
		return true;
	}

	function db_user_registration($username, $first_name, $last_name, $email, $password){
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		
		$data = array(
			'username' => $username,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'password' => $password
		);
		
		if(event::notifyFirst('before_new_db_member', array('data' => $data))){
			$member = dao_members::getMemberByEmail($email);
			$member1 = dao_members::getMemberByScreenname($username);
			
			if(!empty($member)){
				event::notify('db_member_error', array('data' => $data,'error' => 'email'));
				return false;
			}
			else if(!empty($member1)){
				event::notify('db_member_error', array('data' => $data, 'error' => 'username'));
				return false;
			}
			else{
				$member_id = dao_members::createNewUser( '','',$username, $first_name, $last_name, $email, md5($password));
				event::notify('new_db_member', array('member_id' => $member_id, 'data' => $data));
			}
		}
	}
	function db_user_forgot_password($email){
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		if(event::notifyFirst('before_new_db_forgot_password', array('email' => $email))){
			$hash = md5(time().$email.rand());
			dao_members::updateHash($email, $hash);
			$member = dao_members::getMemberByEmail($email);
			
			event::notify('db_forgot_password_success', array('data' => $member));
		}
	}
	function db_user_reset_password($password, $hash){
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		if(empty($password) || empty($hash)){
			event::notify('db_reset_password_failed');
			return false;
		}
		$reset_password = dao_members::resetPassword(md5($password), $hash);
		if($reset_password){
			event::notify('db_reset_password_success');
		}
		else{
			event::notify('db_reset_password_failed');
		}
		
	}
	