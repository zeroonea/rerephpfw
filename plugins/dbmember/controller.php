<?php

function db_user_controller($user, $request, $response){
	global $config;
	
	if($request['router'] == 'db_login'){		
		if(db_init_user($user, $_REQUEST['email'], $_REQUEST['password'])){
			
		}
	}
	else if($request['router'] == 'db_register'){
		if(db_user_registration($_REQUEST['username'], $_REQUEST['first_name'], $_REQUEST['last_name'], $_REQUEST['email'], $_REQUEST['password'])){
		
		}
	}
	else if($request['router'] == 'db_forgot_password'){
		db_user_forgot_password(trim($_REQUEST['email']));
	}
	else if($request['router'] == 'db_reset_password'){
		
		db_user_reset_password($_REQUEST['password'], $_REQUEST['hash']);
	}
	else if($request['router'] == 'logout'){
		sess::unregister();
	}
}event::add('authentication', 'db_user_controller', 100);	