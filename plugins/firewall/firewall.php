<?php

function fw_firewall($user, $request, $response){
	global $config, $routing;
	
	$pass = false;
	$roles = $routing[$request['router']]['roles'];
	if(!empty($roles)){
		foreach($roles as $role){
			if($user->hasRole($role)){
				$pass = true;
				break;
			}
		}
	}else{
		$pass = true;
	}
	
	if(!$pass){
		$redirect = $routing[$request['router']]['redirect'];
		
		if(!empty($redirect)){
			if($redirect == 'home'){
				redirect($config['website']['url']);
			}
		}else{
			header('HTTP/1.0 403 Forbidden');
		}
		exit(0);
	}
}event::add('firewall', 'fw_firewall');