<?php

function fb_user_controller($user, $request, $response){
	global $config, $fb;
	
	include_once dirname(__FILE__).'/api/facebook.php';
	
	if($fb == null){
		$fb = new Facebook(array(
			'appId' => $config['facebook']['app_id'],
			'secret' => $config['facebook']['secret']
		));
	}
	
	if($request['router'] == 'facebook_login'){
		if(!empty($_REQUEST['redirect'])){
			$redirect_url = merge_querystring($_REQUEST['redirect'], array(
				'device' => $_REQUEST['device'],
				'sess_key' => sess::get_key()	
			));
		}
		
		$data = array(
			'device' => $_REQUEST['device'],
			'redirect' => $redirect_url,
			'sess_key' => sess::get_key()
		);
		
		foreach($_REQUEST as $key => $value){
			if(strpos($key, 'fbp_') === 0){
				$data[$key] = $value;
			}
		}
	
		$fb_login_url = $fb->getLoginUrl(array(
			'device' => $_REQUEST['device'],
			'scope' => $config['facebook']['permissions'], 
			'redirect_uri' => merge_querystring(get_url('facebook_login_check'), $data)	
		));
		
		redirect($fb_login_url);
	}else if($request['router'] == 'facebook_login_check'){
		fb_init_user($user);
	}else if($request['router'] == 'logout'){
		
		$fb_signed_request = 'fbsr_'.$fb->getAppId();
		setcookie($fb_signed_request, '', time() - 1000);
		setcookie($fb_signed_request, '', time() - 1000, '/');
		
		sess::unregister();
	}
}event::add('authentication', 'fb_user_controller', 100);	