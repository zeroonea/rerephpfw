<?php

function fb_authentication($user, $request, $response){
	global $config, $fb;
	
	include_once dirname(__FILE__).'/api/facebook.php';
	
	if($fb == null){
		$fb = new Facebook(array(
			'appId' => $config['facebook']['app_id'],
			'secret' => $config['facebook']['secret']
		));
	}
	
	fb_init_user($user, true);
	
}event::add('authentication', 'fb_authentication', 99);	


function fb_is_logged(){
	global $fb;
	try{
		$fb->api('/me');
		return true;
	}catch(FacebookApiException $ex){
		return false;
	}
}

function fb_init_user(&$user, $flag = false){
	global $config, $fb;
	
	if($user->getID() > 0) return;
	
	$fbid = $fb->getUser();
	
	if($fbid){
		
		include_once dirname(__FILE__).'/../../daos/dao.members.php';
		$member = dao_members::getMemberByOpenId($fbid, 'facebook');
		
		if(empty($member)){
			if(!$flag){
				/* Add new facebook user */
				$fbdata = $fb->api('/me');
				$member_id = dao_members::createNewUser($fbid, 'facebook', '', $fbdata['first_name'], $fbdata['last_name'], $fbdata['email'], '', $fbdata['locale']);
				$user->setID($member_id);
				$user->addRole('new_fb_user');
			}
		}else{
			$fbdata = $member;
			$user->setID($member['member_id']);
			$user->setOpenId($fbid);
			$user->removeRole('new_fb_user');
			$user->addRole('fb_user');
		}
		
		$user->setFBData($fbdata);
		$user->removeRole('guest');
		$user->addRole('member');
		$user->addRole('openid_user');
	}
}