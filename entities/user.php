<?php

class user{
	
	/**
     * @var int
     */
	protected $id;
	
	/**
     * @var string
     */
	protected $email;
	
	/**
	 * @var string array
	 */
	protected $roles = array();
	
	/**
     * @var string
     */
	protected $screenname;
	
    /**
     * @var string
     */
    protected $firstname;

    /**
     * @var string
     */
    protected $lastname;
	
	/**
     * @var string
     */
	protected $locale;

    /**
     * @var string
     */
    protected $openid;
	
	/**
     * @var string
     */
	protected $provider;
	
	/**
	 * @var string
	 */
	protected $logouturl;
	
	/**
	 * @var string
	 */
	protected $oauth_token;
	
	/**
	 * @var string
	 */
	protected $oauth_token_secret;
	
	/**
	 * @var string
	 */
	protected $session_key; 
	
	protected $attrs;
	
	protected $avatar;
	
	protected $bio;
	
	/**
     * @return int
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }
	
	/**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }
	
	/**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
	
	/**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
	
	/**
     * @param string $roles
     */
    public function addRoles($roles)
    {
		if($roles == null || !is_array($roles)) return;
		foreach($roles as $i => $role){
			$this->addRole($role);
		}
    }
	
	/**
     * @param string $role
     */
    public function addRole($role)
    {
		if(in_array($role, $this->roles)) return;
        $this->roles[] = $role;
		sess::set('member-roles', $this->roles);
    }
	
	/**
     * @param string $role
     */
    public function removeRole($role)
    {
        foreach($this->roles as $i => $_role){
			if($role == $_role){
				unset($this->roles[$i]);
				break;
			}
		}
		sess::set('member-roles', $this->roles);
    }

    /**
     * @return bool
     */
    public function hasRole($role)
    {
        if(is_array($this->roles) && in_array($role, $this->roles)){
			return true;
		}else return false;
    }
	
	/**
     * @param string $screenname
     */
    public function setScreenname($screenname)
    {
        $this->screenname = $screenname;
    }
	
	/**
     * @return string
     */
    public function getScreenname()
    {
        return $this->screenname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Get the full name of the user (first + last name)
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastname();
    }

    /**
     * @param string $openid
     * @return void
     */
    public function setOpenId($openid)
    {
        $this->openid = $openid;
    }

    /**
     * @return string
     */
    public function getOpenId()
    {
        return $this->openid;
    }
	
	
    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }
	
	    /**
     * @param string $provider
     * @return void
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }
	
	public function getLogoutUrl(){
		return $this->logouturl;
	}
	
	public function setLogoutUrl($logouturl){
		$this->logouturl = $logouturl;
	}
	
    /**
     * @param string $oauth_token
     * @return void
     */
    public function setOauthToken($oauth_token)
    {
        $this->oauth_token = $oauth_token;
    }
	
	/**
     * @return string
     */
    public function getOauthToken()
    {
        return $this->oauth_token;
    }
	
    /**
     * @param string $oauth_token_secret
     * @return void
     */
    public function setOauthTokenSecret($oauth_token_secret)
    {
        $this->oauth_token_secret = $oauth_token_secret;
    }
	
	/**
     * @return string
     */
    public function getOauthTokenSecret()
    {
        return $this->oauth_token_secret;
    }
	
	/**
     * @param string $session_key
     * @return void
     */
    public function setSessionKey($session_key)
    {
        $this->session_key = $session_key;
    }
	
	/**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->session_key;
    }

    /**
	 * Support for facebook
     * @param Array
     */
    public function setFBData($fbdata)
    {
        if (isset($fbdata['id'])) {
            $this->setOpenId($fbdata['id']);
        }
        if (isset($fbdata['first_name'])) {
            $this->setFirstname($fbdata['first_name']);
        }
        if (isset($fbdata['last_name'])) {
            $this->setLastname($fbdata['last_name']);
        }
        if (isset($fbdata['email'])) {
            $this->setEmail($fbdata['email']);
        }
		if (isset($fbdata['locale'])) {
            $this->setLocale($fbdata['locale']);
        }
		if (isset($fbdata['screen_name'])) {
            $this->setScreenname($fbdata['screen_name']);
        }
    }
	
	public function setAttrs($attrs){
		$this->attrs = $attrs;
	}
	
	public function getAttrs(){
		return $this->attrs;
	}
	
	public function getAttr($name){
		return $this->attrs[$name];
	}
	
	public function setAvatar($avatar){
		$this->avatar = $avatar;
	}
	
	public function getAvatar(){
		return $this->avatar;
	}
	
	public function setBio($bio){
		$this->bio = $bio;
	}
	
	public function getBio(){
		return $this->bio;
	}
}