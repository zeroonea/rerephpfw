<?php

require_once dirname(__FILE__).'/rerehttp.php';

class rerewebparser {

    private $url;
    private $cookie;
    private $remove_hyperlinks = true;
    private $remove_hiddenfields = true;
	private $styles = array();
	private $files = array();

    public function __construct($url = '', $cookie = '') {
        $this->url = $url;
        $this->cookie = $cookie;
    }

    public function toJson() {
        return $this->_parse('json');
    }

    public function toSerialize() {
        return $this->_parse('serialize');
    }

    public function toArray() {
        return $this->_parse('array');
    }
	
	public function parse_css_files($parent_url, $ext_css){
		$this->url = $parent_url;
		$results = array();
		foreach($ext_css as $css){
			$tmp = html_entity_decode(urldecode(rerehttp::url_to_absolute($this->url, $css['url'])));
			if (empty($tmp)) continue;
			
			$obj = array();
			$obj['url'] = $tmp;
			$obj['content'] = $this->get_css_content(rerehttp::get($obj['url']));
			$obj['type'] = 'link';
			$obj['media'] = $css['media'];
			
			$this->parse_css($obj);
			
			unset($obj['content']);
			$results[] = $obj;
		}
		
		return $results;
	}

    private function _parse($result_type) {
		
        $html = new simple_html_dom();
        $html->load(rerehttp::get($this->url));
		
        $results = array();
        $results['js'] = array();
		$results['css'] = array();

        /** Correct url of A tag */
        $a_tags = $html->find('a');
        foreach ($a_tags as $a) {
            if ($opts['remove_a_href'] == 1) {
                $a->href = '';
            } else {
                $tmp = $a->href;
                $a->href = rerehttp::url_to_absolute($this->url, $a->href);
                if (!$a->href) {
                    $a->href = $tmp;
                }
            }
        }

        /** Correct url of IMG tag */
        $img_tags = $html->find('img');
        foreach ($img_tags as $img) {
            $tmp = $img->src;
            $img->src = rerehttp::url_to_absolute($this->url, $img->src);
            if (!$img->src) {
                $img->src = $tmp;
            }
        }

        $body = $html->find('body', 0);
        //$body->tag = 'div';
        $body = $body->outertext;

        /** Remove hidden fields */
        if ($opts['remove_hidden_fields'] == 1) {
            $hidden_fields = $html->find('input[type=hidden]');
            foreach ($hidden_fields as $field) {
                $body = str_replace($field->outertext, '', $body);
            }
        }

        /** Store script info and remove script from html */
        $script_tags = $html->find('script');
        foreach ($script_tags as $script) {
            $sid = count($results['js']);
            $scr = &$results['js'][$sid];
            if (!empty($script->src)) {
                $scr['src'] = html_entity_decode(urldecode(rerehttp::url_to_absolute($this->url, $script->src)));
                if (!$scr['src']) {
                    $scr['src'] = $script->src;
                }
            } else {
                $scr['embed'] = base64_encode($script->innertext);
            }
            $body = str_replace($script->outertext, '', $body);
        }

        $body = preg_replace('/[\n]+/', "\n", preg_replace('/<!--(.*)-->/Uis', '', $body));
        $results['html'] = base64_encode($body);

        $this->styles = array();
        $sc = 0;
		
		$embedded_css = $html->find('style, link');
		foreach ($embedded_css as $ele) {
			if($ele->tag == 'style'){
				$obj = array();
				$obj['url'] = $this->url;
				$obj['content'] = $this->get_css_content($ele->innertext);
				$obj['type'] = 'embed';
				$obj['media'] = trim($ele->media);
				
				$this->parse_css($obj);
				
				unset($obj['content']);
				$this->styles[] = $obj;
			}else if($ele->tag == 'link'){
				if (strtolower($ele->rel) != 'stylesheet') continue;
				$tmp = html_entity_decode(urldecode(rerehttp::url_to_absolute($this->url, $ele->href)));
				if (empty($tmp)) continue;
				
				$obj = array();
				$obj['url'] = $tmp;
				$obj['content'] = $this->get_css_content(rerehttp::get($obj['url']));
				$obj['type'] = 'link';
				$obj['media'] = trim($ele->media);
				
				$this->parse_css($obj);
				
				unset($obj['content']);
				$this->styles[] = $obj;
			}
		}
		
		$results['css'] = $this->styles;
		$results['files'] = $this->files;

        if ($result_type == 'array') {
            return $results;
        } else if ($result_type == 'json') {
            header('Content-Type: application/json');
            return json_encode($results);
        } else if ($result_type == 'serialize') {
            return serialize($results);
        }
    }
	
	private function parse_css(&$css_obj){
		$urls = $this->extract_css_urls($css_obj['content']);
		if (is_array($urls['import'])){
			foreach ($urls['import'] as $url) {
				$tmp = html_entity_decode(urldecode(rerehttp::url_to_absolute($css_obj['url'], $url)));
				if (empty($tmp)) continue;
				
				$obj = array();
				$obj['url'] = $tmp;
				$obj['content'] = $this->get_css_content(file_get_contents($obj['url']));
				$obj['type'] = 'import';
				
				$this->parse_css($obj);
				
				unset($obj['content']);
				$this->styles[] = $obj;
			}
		}
				
		$this->parse_css_rules($css_obj);
	}
	
	private function parse_css_rules(&$css_obj){
		
		// Correct property url (in background image)
		$purls = $this->extract_css_urls($css_obj['content']);
		if(is_array($purls['property'])) {
			$pos = 0;
			foreach ($purls['property'] as $purl) {
				$aurl = html_entity_decode(urldecode(rerehttp::url_to_absolute($css_obj['url'], $purl)));
				if (empty($aurl)) continue;
				
				if(!in_array($aurl, $this->files)){
					$this->files[] = $aurl;
				}
				
				$pos = strpos($css_obj['content'], $purl, $pos);
				if ($pos !== false) {
					$css_obj['content'] = substr_replace($css_obj['content'], $aurl, $pos, strlen($purl));
					$pos += strlen($aurl);
				}
			}
		}
				
		// Parse CSS Rules
		$x = 0;
		$y = 0;
		$z = 0;
		$m = 0;
		$n = 0;
		$l = strlen($css_obj['content']);
		$gl = 0;// Group Level
		$is_group = false;
		$parent_rule = null;
		while(true){
			$is_group = false;
			
			$x = strpos($css_obj['content'], '{', $x);
			if($x !== false){
				if($gl == 0){
					$rule = &$css_obj['rules'][];
				}else{
					$rule = &$parent_rule['rules'][];
				}
				
				// Check if is a media group
				$y = strrpos($css_obj['content'], '@', -($l - $x)); 
				// Check if is a selector after an @import
				$z = strrpos($css_obj['content'], ';', -($l - $x)); 
				// Check if is a selector after another selector or after a group
				$m = strrpos($css_obj['content'], '}', -($l - $x));
				// Check if is a selector inside a group
				$n = strrpos($css_obj['content'], '{', -($l - ($x - 1)));
				
				$tmp = max($y, $z, $m, $n);
				switch($tmp){
					case $y:
						// Is a media group
						$gl += 1;
						$is_group = true;
						$parent_rule = &$rule;
					break;
					case $z:
						// Is a selector after an @import
						$y = $z + 1;
					break;
					case $m:
						// Is a selector after another selector or after a group
						$y = $m + 1;
					break;
					case $n:
						// Is a selector inside a group
						$y = $n + 1;
					break;
				}
				
				if($y === false){
					$y = 0;
					$gl = 0;
					$is_group = false;
				}
				
				$rule['n'] = trim(preg_replace('/[\t\n\r]*/', '', substr($css_obj['content'], $y, $x - $y)));
				
				if(!$is_group){
					// Find content
					$z = strpos($css_obj['content'], '}', $x);
					if($z !== false){
						$rule['v'] = trim(preg_replace('/[\t\n\r]*/', '', substr($css_obj['content'], $x + 1, $z - ($x + 1))));
						
						if($gl > 0){
							// Check if end of a group
							$j = 0;
							while(true){
								$c = $css_obj['content'][$z + 1 + $j++];
								if($c == "\n" || $c == " " || $c == "\t" || $c == "\r"){
									
								}else if($c == "}"){
									$gl--;
									if($gl <= 0) break;
								}else{
									break;
								}
							}
						}
					}else{
						break;
					}
				}else{
					$z = strpos($css_obj['content'], '{', $x + 1);
					$m = strpos($css_obj['content'], '}', $x + 1);
					
					$tmp = min($z, $m);
					if($tmp == $z && $z !== false){
						// Group with child rules
					}else if($tmp == $m || $z === false){
						// Group with no child rules
						$gl -= 1;
						$rule['v'] = trim(preg_replace('/[\t\n\r]*/', '', substr($css_obj['content'], $x + 1, $m - ($x + 1))));
					}
				}
				
				$x += 1;
			}else{
				break;
			}
		}
	}

    public function extract_css_urls($text) {
        $urls = array();

        $url_pattern = '(([^\\\\\'", \(\)]*(\\\\.)?)+)';
        $urlfunc_pattern = 'url\(\s*[\'"]?' . $url_pattern . '[\'"]?\s*\)';
        $pattern = '/(' .
                '(@import\s*[\'"]' . $url_pattern . '[\'"])' .
                '|(@import\s*' . $urlfunc_pattern . ')' .
                '|(' . $urlfunc_pattern . ')' . ')/iu';

        if (!preg_match_all($pattern, $text, $matches)) {
            return $urls;
        }

        /** @import '...' */
        /** @import "..." */
        foreach ($matches[3] as $match) {
            if (!empty($match)) {
                $urls['import'][] = preg_replace('/\\\\(.)/u', '\\1', $match);
            }
        }

        /** @import url(...) */
        /** @import url('...') */
        /** @import url("...") */
        foreach ($matches[7] as $match) {
            if (!empty($match)) {
                $urls['import'][] = preg_replace('/\\\\(.)/u', '\\1', $match);
            }
        }

        /** url(...) */
        /** url('...') */
        /** url("...") */
        foreach ($matches[11] as $match) {
            if (!empty($match)) {
                $urls['property'][] = preg_replace('/\\\\(.)/u', '\\1', $match);
            }
        }

        return $urls;
    }
	
	private function get_css_content($buffer){
		$regex = array(
			'!/\*[^*]*\*+([^/][^*]*\*+)*/!' => '',
			'!\*/!' => '',
			'!/\*!' => ''
		);
		return preg_replace(array_keys($regex),$regex,$buffer);
	}
}
