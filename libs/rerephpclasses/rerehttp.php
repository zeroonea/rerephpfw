<?php

class rerehttp{

	private static $user_agents = array(
	"Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051002 Firefox/1.6a1",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-us) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) Speedy Spider (http://www.entireweb.com/about/search_tech/speedy_spider/)",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.11) Gecko/20100701 Firefox/3.5.11 ( .NET CLR 3.5.30729)",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 ( .NET CLR 3.5.30729)",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ro; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.8) Gecko/20100722 AskTbGOM2/3.7.0.231 Firefox/3.6.8",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; ro; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8",
	"Mozilla/5.0 (en-us) AppleWebKit/525.13 (KHTML, like Gecko; Google Wireless Transcoder) Version/3.1 Safari/525.13",
	"Mozilla/5.0 (en-us) AppleWebKit/525.13 (KHTML, like Gecko; Google Wireless Transcoder) Version/3.1 Safari/525.13,Mozilla/5.0 (en-us) AppleWebKit/525.13 (KHTML, like Gecko; Google Wireless Transcoder) Version/3.1 Safari/525.13",
	"Opera/9.80 (Windows NT 5.1; U; ro) Presto/2.6.30 Version/10.60",
	"Opera/9.80 (Windows NT 6.1; U; en) Presto/2.2.15 Version/10.10");

	public static function send_request($type, $url, $params = '', $referer = '', $cookie = '', $cookie_file = '', $cookie_jar = '', $user_agent = '', $proxy = false, $header = false, $body = true, $custom_headers = array('Connection: Keep-Alive', 'Keep-Alive: 300')){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $custom_headers);
		
		if(!empty($proxy)){
			if(is_array($proxy)){
				$proxy = $proxy[rand(0, count($proxy) - 1)];
			}
			curl_setopt($ch, CURLOPT_PROXY, $proxy);
		}
		
		if(empty($user_agent)){
			$user_agent = self::$user_agents;
		}
		if(is_array($user_agent)){
			$user_agent = $user_agent[rand(0, count($user_agent) - 1)];
		}
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

		if (!empty($cookie_file)) {
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
		} else if (!empty($cookie)) {
			curl_setopt($ch, CURLOPT_COOKIE, $cookie);
		}
		
		if(!empty($cookie_jar)){
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_jar);
		}
		
		curl_setopt($ch, CURLOPT_REFERER, $referer);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_NOBODY, !$body);
		
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		
		curl_setopt($ch, CURLOPT_ENCODING , "gzip");
		curl_setopt($ch, CURLOPT_BUFFERSIZE, 32768);
		
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, false);
		
		curl_setopt($ch, $type == 'POST' ? CURLOPT_POST : CURLOPT_HTTPGET, 1);
		
		if ($type == 'POST') {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}
		curl_setopt($ch, CURLOPT_HEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$response = curl_exec($ch);
		
		if (curl_errno($ch)) {
			return false;
		}
		
		curl_close($ch);
		return $response;
	}

	public static function post($url, $params, $cookie_string = '', $cookie_file = '', $cookie_jar = '', $user_agent = '', $proxy = false, $header = false, $body = true, $custom_headers = null){
		return self::send_request('POST', $url, $params, $url, $cookie_string, $cookie_file, $cookie_jar, $user_agent, $proxy, $header, $body, $custom_headers);
	}

	public static function get($url, $cookie_string = '', $cookie_file = '', $cookie_jar = '', $user_agent = '', $proxy = false){
		return self::send_request('GET', $url, '', $url, $cookie_string, $cookie_file, $cookie_jar, $user_agent, $proxy);
	}

	public static function url_to_absolute($baseUrl, $relativeUrl){
		$r = self::split_url($relativeUrl);
		
		if ($r === FALSE)
			return FALSE;
		if (!empty($r['scheme'])) {
			if (!empty($r['path']) && $r['path'][0] == '/')
				$r['path'] = self::url_remove_dot_segments($r['path']);
			return self::join_url($r);
		}
		$b = self::split_url($baseUrl);
		if ($b === FALSE || empty($b['scheme']) || empty($b['host']))
			return FALSE;
		$r['scheme'] = $b['scheme'];
		
		if (isset($r['host'])) {
			if (!empty($r['path']))
				$r['path'] = self::url_remove_dot_segments($r['path']);
			return self::join_url($r);
		}
		unset($r['port']);
		unset($r['user']);
		unset($r['pass']);
		
		$r['host'] = $b['host'];
		if (isset($b['port']))
			$r['port'] = $b['port'];
		if (isset($b['user']))
			$r['user'] = $b['user'];
		if (isset($b['pass']))
			$r['pass'] = $b['pass'];
		
		if (empty($r['path'])) {
			if (!empty($b['path']))
				$r['path'] = $b['path'];
			if (!isset($r['query']) && isset($b['query']))
				$r['query'] = $b['query'];
			return self::join_url($r);
		}
		
		if ($r['path'][0] != '/') {
			$base = mb_strrchr($b['path'], '/', TRUE, 'UTF-8');
			if ($base === FALSE)
				$base = '';
			$r['path'] = $base . '/' . $r['path'];
		}
		$r['path'] = self::url_remove_dot_segments($r['path']);
		return self::join_url($r);
	}

	public static function url_remove_dot_segments($path){
		$inSegs  = preg_split('!/!u', $path);
		$outSegs = array();
		foreach ($inSegs as $seg) {
			if ($seg == '' || $seg == '.')
				continue;
			if ($seg == '..')
				array_pop($outSegs);
			else
				array_push($outSegs, $seg);
		}
		$outPath = implode('/', $outSegs);
		if ($path[0] == '/')
			$outPath = '/' . $outPath;
		if ($outPath != '/' && (mb_strlen($path) - 1) == mb_strrpos($path, '/', 'UTF-8'))
			$outPath .= '/';
		return $outPath;
	}

	public static function join_url($parts, $encode = TRUE){
		if ($encode) {
			if (isset($parts['user']))
				$parts['user'] = rawurlencode($parts['user']);
			if (isset($parts['pass']))
				$parts['pass'] = rawurlencode($parts['pass']);
			if (isset($parts['host']) && !preg_match('!^(\[[\da-f.:]+\]])|([\da-f.:]+)$!ui', $parts['host']))
				$parts['host'] = rawurlencode($parts['host']);
			if (!empty($parts['path']))
				$parts['path'] = preg_replace('!%2F!ui', '/', rawurlencode($parts['path']));
			if (isset($parts['query']))
				$parts['query'] = rawurlencode($parts['query']);
			if (isset($parts['fragment']))
				$parts['fragment'] = rawurlencode($parts['fragment']);
		}
		
		$url = '';
		if (!empty($parts['scheme']))
			$url .= $parts['scheme'] . ':';
		if (isset($parts['host'])) {
			$url .= '//';
			if (isset($parts['user'])) {
				$url .= $parts['user'];
				if (isset($parts['pass']))
					$url .= ':' . $parts['pass'];
				$url .= '@';
			}
			if (preg_match('!^[\da-f]*:[\da-f.:]+$!ui', $parts['host']))
				$url .= '[' . $parts['host'] . ']';
			else
				$url .= $parts['host'];
			if (isset($parts['port']))
				$url .= ':' . $parts['port'];
			if (!empty($parts['path']) && $parts['path'][0] != '/')
				$url .= '/';
		}
		if (!empty($parts['path']))
			$url .= $parts['path'];
		if (isset($parts['query']))
			$url .= '?' . $parts['query'];
		if (isset($parts['fragment']))
			$url .= '#' . $parts['fragment'];
		return $url;
	}

	public static function split_url($url, $decode = TRUE){
		$xunressub = 'a-zA-Z\d\-._~\!$&\'()*+,;=';
		$xpchar    = $xunressub . ':@%';
		$xscheme = '([a-zA-Z][a-zA-Z\d+-.]*)';
		$xuserinfo = '(([' . $xunressub . '%]*)' . '(:([' . $xunressub . ':%]*))?)';
		$xipv4 = '(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})';
		$xipv6 = '(\[([a-fA-F\d.:]+)\])';
		$xhost_name = '([a-zA-Z\d-.%]+)';
		$xhost      = '(' . $xhost_name . '|' . $xipv4 . '|' . $xipv6 . ')';
		$xport      = '(\d*)';
		$xauthority = '((' . $xuserinfo . '@)?' . $xhost . '?(:' . $xport . ')?)';
		$xslash_seg    = '(/[' . $xpchar . ']*)';
		$xpath_authabs = '((//' . $xauthority . ')((/[' . $xpchar . ']*)*))';
		$xpath_rel     = '([' . $xpchar . ']+' . $xslash_seg . '*)';
		$xpath_abs     = '(/(' . $xpath_rel . ')?)';
		$xapath        = '(' . $xpath_authabs . '|' . $xpath_abs . '|' . $xpath_rel . ')';
		$xqueryfrag = '([' . $xpchar . '/?' . ']*)';
		$xurl = '^(' . $xscheme . ':)?' . $xapath . '?' . '(\?' . $xqueryfrag . ')?(#' . $xqueryfrag . ')?$';
		if (!preg_match('!' . $xurl . '!', $url, $m))
			return FALSE;
		if (!empty($m[2]))
			$parts['scheme'] = strtolower($m[2]);
		if (!empty($m[7])) {
			if (isset($m[9]))
				$parts['user'] = $m[9];
			else
				$parts['user'] = '';
		}
		if (!empty($m[10]))
			$parts['pass'] = $m[11];
		if (!empty($m[13]))
			$h = $parts['host'] = $m[13];
		else if (!empty($m[14]))
			$parts['host'] = $m[14];
		else if (!empty($m[16]))
			$parts['host'] = $m[16];
		else if (!empty($m[5]))
			$parts['host'] = '';
		if (!empty($m[17]))
			$parts['port'] = $m[18];
		if (!empty($m[19]))
			$parts['path'] = $m[19];
		else if (!empty($m[21]))
			$parts['path'] = $m[21];
		else if (!empty($m[25]))
			$parts['path'] = $m[25];
		if (!empty($m[27]))
			$parts['query'] = $m[28];
		if (!empty($m[29]))
			$parts['fragment'] = $m[30];
		if (!$decode)
			return $parts;
		if (!empty($parts['user']))
			$parts['user'] = rawurldecode($parts['user']);
		if (!empty($parts['pass']))
			$parts['pass'] = rawurldecode($parts['pass']);
		if (!empty($parts['path']))
			$parts['path'] = rawurldecode($parts['path']);
		if (isset($h))
			$parts['host'] = rawurldecode($parts['host']);
		if (!empty($parts['query']))
			$parts['query'] = rawurldecode($parts['query']);
		if (!empty($parts['fragment']))
			$parts['fragment'] = rawurldecode($parts['fragment']);
		return $parts;
	}
}