<?php

class dao_sessions{

	public static function getSessionByKey($key){
		global $db;               
        return $db->query_first(sprintf('SELECT * FROM %s WHERE session_key=\'%s\' LIMIT 1',$db->pre.'sessions',$db->escape($key)));
	}	
	
	public static function deleteSessionByKey($key){
		global $db;		
		return $db->query(sprintf('DELETE FROM %s WHERE session_key = \'%s\'',
                 $db->pre.'sessions',
                 $db->escape($key)));
	}
	
	public static function updateSessionByKey($key, $sess_data){
		global $db;
		$data['data'] = $sess_data;		
		$data['updated'] = 'UTC_TIMESTAMP()';
		
        return $db->update($db->pre.'sessions',$data,sprintf('session_key=\'%s\'', $db->escape($key))); 
	}
	
	public static function createSession($key, $sess_data, $device = ''){
		global $db;
		$data['session_key'] = $key;
		$data['data'] = $sess_data;
		$data['device'] = $device;
		$data['created'] = 'UTC_TIMESTAMP()';		
        return $db->insert($db->pre.'sessions', $data, true);
	}
}