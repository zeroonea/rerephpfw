<?php
/**
 * 
 * @author Tin Nguyen
 */
class dao_options{

    /**
     * Get single or multiple option
     *
     * @param string|array      $name
     * @return mix
     */
    public static function get_options($name){
        global $db;
        $where = array();
        if(is_array($name)){
            $where[] = "option_name IN ('".implode("', '", $name)."')";
        }else{
            $where[] = sprintf("option_name = '%s'", $db->escape($name));
        }
        if(count($where) > 0){
            $where = implode(' AND ', $where);
        }else $where = '';

        if(is_array($name)){
            return $db->fetch_all_array(sprintf("SELECT option_name, option_data FROM %s 
                WHERE %s LIMIT %d",
                $db->pre . 'options', $where, count($name)));
        }else{
            return $db->query_first(sprintf("SELECT option_name, option_data FROM %s 
                WHERE %s LIMIT 1",
                $db->pre . 'options', $where));
        }
    }

    /**
     * Save (insert or update) option value
     *
     * @param string    $name
     * @param mix       $value
     * @param int       $member_id
     * @return option id
     */
    public static function save_option($name, $value, $member_id){
        global $db;
        $opt = $db->query_first(sprintf("SELECT * FROM %s
			WHERE option_name = '%s'",
                            $db->pre . 'options', $db->escape($name)));

        if (empty($opt['option_id'])) {
            return $db->insert($db->pre . 'options',
                array(
                    'option_data' => $value,
                    'option_name' => $name,
                    'option_creator' => $member_id
                )
            );
        } else {
            $db->update($db->pre . 'options', array('option_data' => $value),
                    sprintf("option_creator = '%d' AND option_name = '%s'", $db->escape($member_id), $db->escape($name)));
            return true;
        }
    }

    /**
     * Remove option
     *
     * @param string    $option_name
     * @return int
     */
    public static function remove_option($option_name){
        global $db;
        $db->query(sprintf("DELETE FROM %s WHERE option_name = '%s'",
                    $db->pre . 'options', $db->escape($option_name)));
        return $db->affected_rows;
    }
}