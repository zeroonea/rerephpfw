<?php

class dao_members{

	public static function getMemberByID($id){
		global $db;

        $member = $db->query_first(sprintf("SELECT * FROM %s
            WHERE member_id = '%d' LIMIT 1",
                $db->pre.'members',
                $db->escape($id)));
        
        return $member;
	}
	
	public static function getMemberByEmail($email){
		global $db;

        $member = $db->query_first(sprintf('SELECT * FROM %s
            WHERE email = \'%s\' LIMIT 1',
                $db->pre.'members',
                $db->escape($email)));
        
        return $member;
	}
	public static function getMemberByScreenname($screen_name){
		global $db;

        $member = $db->query_first(sprintf('SELECT * FROM %s
            WHERE screen_name = \'%s\' LIMIT 1',
                $db->pre.'members',
                $db->escape($screen_name)));
        
        return $member;
	}
	public static function getMemberByOpenID($openid, $provider){
		global $db;

        $member = $db->query_first(sprintf("SELECT * FROM %s
            WHERE openid = '%s' AND provider = '%s' LIMIT 1",
                $db->pre.'members',
                $db->escape($openid),
				$db->escape($provider)));
        
        return $member;
	}
	
	public static function createNewUser($openid, $provider, $screen_name = '', $first_name = '', $last_name = '', $email = '', $password = '', $locale = '', $avatar = ''){
		global $db;
		//$data['member_id'] = 'UUID()';
		$data['openid'] = $openid;
		$data['provider'] = $provider;
		$data['screen_name'] = $screen_name;
		$data['first_name'] = $first_name;
		$data['last_name'] = $last_name;
        $data['email'] = $email;
		$data['password'] = $password;
		$data['locale'] = $locale;
		$data['avatar'] = $avatar;
		$data['created'] = 'UTC_TIMESTAMP()';
		
        return $db->insert($db->pre.'members', $data, true);
	}
	public static function updateHash($email, $hash){
		global $db;
		$data['hash'] = $hash;		
		return $db->update($db->pre.'members',$data, sprintf('email= \'%s\'', $db->escape($email)));
	}
	public static function updateAttrs($member_id, $attrs){
		global $db;
		$data['attrs'] = $attrs;		
		return $db->update($db->pre.'members', $data, sprintf('member_id= %d', $db->escape($member_id)));
	}
	public static function updateScreenname($member_id, $screen_name){
		global $db;
		$data['screen_name'] = $screen_name;		
		return $db->update($db->pre.'members',$data, sprintf('member_id= %d', $db->escape($member_id)));
	}
	public static function resetPassword($password, $hash){
		global $db;
		$data['password'] = $password;
		$data['hash'] = '';	
		return $db->update($db->pre.'members',$data, sprintf('hash= \'%s\'', $db->escape($hash)));
	}
	public static function updateProfile($member_id, $first_name, $last_name, $bio){
		global $db;
		$data['first_name'] = $first_name;
		$data['last_name'] = $last_name;
		$data['bio'] = $bio;
				
		return $db->update($db->pre.'members',$data, sprintf('member_id= %d', $db->escape($member_id)));
	}
}